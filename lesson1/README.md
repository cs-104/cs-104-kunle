EXECUTION PROCESS

#Set executable permission to “cpumemdisk.sh” file.
  chmod +x  /cpumemdisk.sh
 #RUN 
 sh cpumemdisk.sh
 
 #Add a cronjob to automate this on command line. It runs daily at 11AM and redirect output to a text file on the homepath of my server
 crontab -e
 #ADD LINE BELOW TO THE FILE THAT POPS UP and save {:wq!}
 0 11 * * * /home/kunle/vimcpumemdisk.sh >> /home/kunle/vimcpumememdisk_report.log 2>&1 


ON JENKINS SET 
Build periodically

 H 11 * * 1-5
 
 1-5 means monday to friday at 11am


THE BASH SCRIPT BELOW COULD ALSO BE USED TO CAPTURE SYSTEM INFORMATION
#Kunle solanke
#!/usr/bin/env bash
#########################################
# Capturing System Information
###################################
# define variable for information
host_name=`hostname -f`
date_report=`date`
sleep 20s
echo ""
echo ""
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
echo -e "Inventory report for the $host_name as at $date_report"
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
echo ""
echo ""
echo "RAM              : `free -h | awk  '/Mem:/{print $2}'`
Bash version           : `bash --version | head -1 | awk '{print $4}'`
Java version           : `java -version 2>&1 | head -1 | awk '{print $NF}' | sed 's/\"//g'`
Operating System       : `free -m`
Operating System       : `uname -s`
OS version             : `uname -v`
Currently connected    : `w`
uptime                 : `uptime -p`
Last date of log in    : `last -a | head -3`
Process Statistics     : `vmstat 1 5`"
echo ""
echo ""
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
echo ""
echo ""
\# Hardware Information
lshw -short
echo ""
echo ""
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------"
