variable "aws_ami" {
  default = ""
}

variable "server_type" {
  default = ""
}

variable "target_keypairs" {
  default = ""
}

variable "acct_vpc_id" {
    default= ""
}

variable "acct_subnet_id" {
    default = ""
}

variable "env_name" {
    default = ""
}

variable "bucket_name" {}

variable "acl_value" {
    default = "public-read-write"
}


variable "iam-username" {
    default = ""
}

variable "tag-user" {
    default = ""
}
