provider "aws" {
region = "eu-west-1"
profile =[terraform.workspace]
}
module "iam" {
    source = "git::https://kunle008@bitbucket.org/cs-104/cs-104-kunle.git//lesson7b-jenkins//iam/iam-users"
    iam-username = "kunle-test-2"
    bucket_name = "kunle-iam-${terraform.workspace}"
	tag-user =  "kunle-iam-${terraform.workspace}"
}
