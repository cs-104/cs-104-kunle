provider "aws" {
  version = "~> 3.0"
 region = "eu-west-1"
}

resource "aws_ami_from_instance""launch_ami" {
  name               = "cs-104-kunle-ami-${formatdate("YYYYMMDDhhmmss", timestamp())}"
  source_instance_id = "i-0084342368c67e045"
  
  tags = {

name = "New AMI IMAGE from instance"

}
}

 variable "account" {
    type = list(string)
    default = ["984463041714", "023451010066","197064613889"]

    }
resource "aws_ami_launch_permission" "share_ami" { 
  image_id   = aws_ami_from_instance.launch_ami.id
  count         = length(var.account) //count will be 3
  account_id = var.account[count.index]
}

